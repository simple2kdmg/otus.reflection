﻿using System;
using otus.reflection.Models;

namespace otus.reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInstance = new TestClass(15, "Test");
            var serializer = new CustomSerializer<TestClass>();

            var serialized = serializer.Serialize(testInstance);
            Console.WriteLine(serialized + Environment.NewLine);

            var deserialized = serializer.Deserialize(serialized);
            deserialized.GetInfo();
            Console.WriteLine(deserialized.GetDefault() + Environment.NewLine);

            SerializerBenchmark.CompareWithNewtonSoft(serializer, testInstance, 10000);
        }
    }
}
