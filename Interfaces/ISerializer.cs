﻿namespace otus.reflection.Interfaces
{
    public interface ISerializer<T>
    {
        string Serialize(T target);
        T Deserialize(string target);
    }
}