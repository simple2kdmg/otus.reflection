﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using otus.reflection.Extensions;
using otus.reflection.Interfaces;

namespace otus.reflection.Models
{
    public class CustomSerializer<T> : ISerializer<T> where T : new()
    {
        private readonly char _separator;

        public CustomSerializer(char separator = ';')
        {
            _separator = separator;
        }

        public string Serialize(T target)
        {
            if (target == null) throw new ArgumentNullException(nameof(target));

            StringBuilder result = new StringBuilder();

            foreach (var pInfo in typeof(T).GetProperties())
            {
                result.Append(SerializeProperty(pInfo, target));
            }

            return result.ToString();
        }

        public T Deserialize(string target)
        {
            if (String.IsNullOrEmpty(target)) throw new ArgumentNullException(nameof(target));

            var deserializedProperties = GetDeserializedProperties(target);
            T result = new T();

            foreach (var info in typeof(T).GetProperties())
            {
                var propertyValue = deserializedProperties.GetValueOrDefault(info.Name, null);
                info.SetValue(result, propertyValue.ParseValue(info.PropertyType));
            }

            return result;
        }

        private string SerializeProperty(PropertyInfo info, T target)
        {
            return $"{info.Name} {info.GetValue(target)}{_separator}";
        }

        private Dictionary<string, string> GetDeserializedProperties(string target)
        {
            var result = new Dictionary<string, string>();

            foreach (var deserializedProperty in target.Split(_separator))
            {
                if (String.IsNullOrEmpty(deserializedProperty)) continue;

                var splitted = deserializedProperty.Split(' ');
                result.Add(splitted[0], splitted[1]);
            }

            return result;
        }
    }
}