﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;
using otus.reflection.Interfaces;

namespace otus.reflection.Models
{
    public class SerializerBenchmark
    {
        public static void CompareWithNewtonSoft<T>(ISerializer<T> serializer, T instance, long iterationCount = 100)
        {
            Stopwatch sw = Stopwatch.StartNew();

            for (int i = 0; i < iterationCount; i++)
            {
                serializer.Serialize(instance);
            }
            sw.Stop();
            Console.WriteLine($"Custom serializer serialize time: {(double)(sw.ElapsedMilliseconds) / iterationCount} ms.{Environment.NewLine}");

            sw.Start();
            var customSerialized = serializer.Serialize(instance);
            for (int i = 0; i < iterationCount; i++)
            {
                serializer.Deserialize(customSerialized);
            }
            sw.Stop();
            Console.WriteLine($"Custom serializer deserialize time: {(double)(sw.ElapsedMilliseconds) / iterationCount} ms.{Environment.NewLine}");

            sw.Start();
            for (int i = 0; i < iterationCount; i++)
            {
                JsonConvert.SerializeObject(instance);
            }
            sw.Stop();
            Console.WriteLine($"NewtonSoft serializer serialize time: {(double)(sw.ElapsedMilliseconds) / iterationCount} ms.{Environment.NewLine}");

            sw.Start();
            var newtonSerialized = JsonConvert.SerializeObject(instance);
            for (int i = 0; i < iterationCount; i++)
            {
                JsonConvert.DeserializeObject<T>(newtonSerialized);
            }
            sw.Stop();
            Console.WriteLine($"NewtonSoft serializer deserialize time: {(double)(sw.ElapsedMilliseconds) / iterationCount} ms.{Environment.NewLine}");
        }
    }
}