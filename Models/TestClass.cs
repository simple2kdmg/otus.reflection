﻿using System;

namespace otus.reflection.Models
{
    public class TestClass
    {
        public int? Id { get; set; }
        public string Name { get; set; }

        public TestClass()
        {
            
        }

        public TestClass(int? id, string name)
        {
            Id = id;
            Name = name;
        }

        public TestClass GetDefault()
        {
            return new TestClass(null, "Undefined");
        }

        public void GetInfo()
        {
            Console.WriteLine($"Id: {Id}, Name: {Name}");
        }
    }
}